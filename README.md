This repository showcases how to combine request / response paradigm (http requests) with producer / consumer paradigm (kafka producers and consumers).
The idea is to receive an http request, push a message into a kafka topic and wait for a result in another kafka topic. Once the response is received,
the http request is going to be ended.

# Getting started

```bash
cd deployment
docker-compose build
docker-compose up
```

## Local components

At the moment, we haven't dockerized the business logic components. In order to run them:

```
cd project
./gradlew build
java -jar kafka-streams/build/libs/kafka-streams.jar

# Open a new terminal
cd webapi
./start.sh
```