package com.veridiumid.samples.kafka.streams.processors;

import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;

/**
 * Provides a simple processor which appends a string to every processed message.
 */
public class AppenderProcessor implements Processor<String, String> {
    private ProcessorContext context;
    private String suffix;

    public AppenderProcessor(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public void process(String key, String value) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException iex) {
            iex.printStackTrace(System.err);
        }

        System.out.println(String.format("Processing request: %s, currSuffix: %s.", value, suffix));
        context.forward(key, String.format("%s:%s", value, suffix));
    }

    @Override
    public void close() { }
}
