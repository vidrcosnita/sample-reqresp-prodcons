package ioc.modules

import java.util.Properties

import com.google.inject.{AbstractModule, Provides}
import com.typesafe.config.Config
import javax.inject.Singleton
import kafka.{ConsumerToHttpBridge, KafkaConsumerToHttpBridge}
import net.codingwell.scalaguice.ScalaModule
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.apache.kafka.common.serialization.Serdes.StringSerde

import scala.concurrent.ExecutionContext

class AppModule extends AbstractModule with ScalaModule {
  private implicit val ec: ExecutionContext = ExecutionContext.global

  override def configure: Unit = { }

  @Singleton
  @Provides
  def kafkaProducer(): KafkaProducer[String, String] = {
    val serializerName = (new StringSerde).serializer().getClass.getName
    val props = new Properties
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "webapi-http-config")
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, serializerName)
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, serializerName)
    return new KafkaProducer[String, String](props)
  }

  @Singleton
  @Provides
  def httpBridge(cfg: Config): ConsumerToHttpBridge = {
    val topicName = cfg.getString("webapi.kafka.httpTopicResults")
    val bridge = new KafkaConsumerToHttpBridge(topicName)
    bridge.start()
    bridge
  }
}