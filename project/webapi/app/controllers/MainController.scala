package controllers

import java.util.UUID

import com.typesafe.config.Config
import javax.inject.Inject
import kafka.ConsumerToHttpBridge
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import play.api.mvc.{AbstractController, ControllerComponents, Result}

import scala.concurrent.{ExecutionContext, Future}

class MainController @Inject()(cc: ControllerComponents, config: Config, producer: KafkaProducer[String, String],
                               httpBridge: ConsumerToHttpBridge)
                              (implicit ec: ExecutionContext) extends AbstractController(cc) {
  private lazy val producerTopic = config.getString("webapi.kafka.httpTopic")

  def testRoute = Action.async {
    val uid = UUID.randomUUID.toString
    val pendingResult = producer.send(new ProducerRecord[String, String](producerTopic, uid, uid + ":content"))
    Future { pendingResult.get }.flatMap(_ => httpBridge.bridge(uid)).map(r => Ok(r))
  }
}