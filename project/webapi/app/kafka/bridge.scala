package kafka

import java.time.Duration
import java.util.concurrent.ConcurrentHashMap
import java.util.{Collections, Properties, UUID}

import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.common.serialization.Serdes.StringSerde

import scala.concurrent.{ExecutionContext, Future, Promise}
import collection.JavaConverters._

/**
  * Provides a simple contract for adapting http request/response paradigm to a producer/consumer paradigm. It lets the
  * developer correlates the results of a messaging bus with a pending http result.
  */
sealed trait ConsumerToHttpBridge {
  def start(): Future[Unit]
  def bridge(reqId: String): Future[String]
}

sealed class KafkaConsumerToHttpBridge(topicName: String)(implicit ec: ExecutionContext) extends ConsumerToHttpBridge {
  private val consumerID = UUID.randomUUID.toString
  private lazy val stringDeserializerName = (new StringSerde).deserializer().getClass.getName
  private var pendingRequests = new ConcurrentHashMap[String, (String) => Unit]()

  private lazy val kafkaConsumer: KafkaConsumer[String, String] = {
    val props = new Properties
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerID)
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, stringDeserializerName)
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, stringDeserializerName)
    props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1000")
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
    val consumer = new KafkaConsumer[String, String](props)
    consumer.subscribe(Collections.singletonList(topicName))
    consumer
  }


  override def start(): Future[Unit] = {
    Future {
      while (true) {
        kafkaConsumer.poll(KafkaConsumerToHttpBridge.pollTimeout).asScala
          .filter(rec => {
            pendingRequests.containsKey(rec.key())
          })
          .map(rec => (pendingRequests.get(rec.key()), rec.value()))
          .foreach {
            case Tuple2(fn, value) => {
              fn(value)
            }
          }
      }
    }
  }

  override def bridge(reqId: String): Future[String] = {
    val pendingResult = Promise[String]()

    pendingRequests.put(reqId, (result: String) => {
      pendingRequests.remove(reqId)
      pendingResult.success(result)
    })

    pendingResult.future
  }
}

object KafkaConsumerToHttpBridge {
  final val pollTimeout = Duration.ofMillis(1000)
}